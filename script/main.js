function doit() {

    const url = 'https://www.breakingbadapi.com/api/';
    let artist = document.getElementById('artist').value;
    let imgDiv = document.getElementById('img');
    let rightDiv = document.getElementById('info');

    imgDiv.innerHTML = '';
    rightDiv.innerHTML = '';

    let newUrl = url.concat('characters?name=' + artist);
    let ul = document.createElement('ul');

    fetch(newUrl)
        .then(response => {
            return response.json();
        })
        .then(data => {
            for(let key in data[0]) {
                let li = document.createElement('li');
                let span = document.createElement('span');
                span.style.fontSize = '20px';
                if(key === 'img') {
                    let image = document.createElement('img');
                    image.style.width = '300px';
                    image.src = `${data[0][key]}`;
                    imgDiv.append(image);
                    continue;
                }
                let newKey = key[0].toUpperCase() + key.slice(1);
                span.innerHTML = `<b>${newKey}</b> : ${data[0][key]}`;
                li.append(span);
                ul.append(li);
                rightDiv.appendChild(ul);

            }
        })
}
